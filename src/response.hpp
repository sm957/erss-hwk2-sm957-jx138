#include <string>
#include <vector>
#include <unordered_map>

class Response {
public:
    // Constructors
    Response(){is_null = true;};
    Response(const std::string& raw_response){
        parse(raw_response);
        raw_response_ = raw_response;
    };
    // Getters
    std::string get_version() const { return version_; }
    std::string get_condition() const { return condition_; }
    std::string get_phrase() const { return phrase_; }
    std::vector<std::string> get_headers() const { return headers_; }
    std::string get_body() const { return body_; }
    void parse(const std::string& raw_response);

    std::string get_etag() const { return etag_; }

    std::string get_transfer_encoding() const { return transfer_encoding_; }
    std::string get_content_type() const { return content_type_; }
    std::string get_raw_response() const { return raw_response_; }
    std::string get_status_line() const { return first_line; }
    void set_message(std::string message){raw_response_=message;}

    int get_content_len() const { return content_len_; }
    bool get_is_private() const { return is_private; }
    bool get_is_revalidate() const { return is_revalidate; }
    bool get_is_nocache() const { return is_nocache; }
    bool get_is_nostore() const { return is_nostore; }
 
    bool get_is_fresh() const { return is_fresh; }
    bool get_is_chunked() const { return is_chunked; }
    bool get_need_valiadate() const { return need_valiadate; }
    time_t get_date() const { return date_; }
    time_t get_expire_t() const { return expire_t; }
    long get_max_age_()const{return max_age_;}
    long get_s_max_age_()const{return s_max_age_;}
    time_t get_cur_age() const { return cur_age; }
    bool is_null = false;
    time_t last_modify;
    std::string cache_mode;
    std::unordered_map<std::string, std::string> headers_map;
private:
    // Private helper methods
    void parse_headers(const std::string& headers_str);
    void parse_header_map(const std::vector<std::string>& headers);
    time_t convert_time(const std::string& time_str);
    void control_cache(const std::string cache_mode);
    void time_manage();
    void checkValidation();
    // Private member variables
    std::string version_;
    std::string condition_;
    std::string phrase_;
    std::vector<std::string> headers_;
    

    std::string body_;
    std::string etag_;
    std::string if_none_match;
    std::string cache_control_;
    std::string transfer_encoding_;
    std::string content_type_;
    std::string expire_;
    std::string raw_response_;
    std::string first_line;
    long max_age_;
    long s_max_age_;
    int content_len_=-1;
   
    bool is_private=false;
    bool is_revalidate = false;
    bool is_nocache = false;
    bool is_nostore = false;
    bool is_maxage = false;
    bool is_fresh= true;
    bool is_chunked = false;
    bool need_valiadate = true;
    time_t date_;
    time_t expire_t;
   
    time_t cur_age;
    
};
