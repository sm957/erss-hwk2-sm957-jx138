#include "response.hpp"


class InvalidResponse : public std::exception {
public:
    virtual const char* what() const noexcept {
        return "Invalid response";
    }
};

void Response::parse(const std::string& raw) {
    // Find the position of the start of the body of the response
    size_t body_pos = raw.find("\r\n\r\n");
    if (body_pos == std::string::npos) {
        throw InvalidResponse();
    }

    // Extract the status line and headers
    std::string headers_str = raw.substr(0, body_pos);
    size_t pos = headers_str.find("\r\n");
    if (pos == std::string::npos) {
        throw InvalidResponse();
    }
    first_line=headers_str.substr(0, pos);
    std::string status_line = headers_str.substr(0, pos);
    headers_str = headers_str.substr(pos + 2);

    // Parse the status line
    pos = status_line.find(' ');
    if (pos == std::string::npos) {
        throw InvalidResponse();
    }
    version_ = status_line.substr(0, pos);
    status_line = status_line.substr(pos + 1);

    pos = status_line.find(' ');
    if (pos == std::string::npos) {
        throw InvalidResponse();
    }
    condition_ = status_line.substr(0, pos);
    phrase_ = status_line.substr(pos + 1);

    parse_headers(headers_str);
    body_ = raw.substr(body_pos + 4);
    parse_header_map(headers_);
    time_manage();

}

void Response::parse_headers(const std::string& headers_str) {
    size_t start = 0, end = 0;
    while (end != std::string::npos) {
        end = headers_str.find("\r\n", start);
        std::string header = headers_str.substr(start, end - start);

        if (header.empty()) {
            break;
        }

        headers_.push_back(header);
        start = end + 2;
    }
}

void Response::control_cache(const std::string cache_mode){
    if (cache_mode.find("private")!=std::string::npos) {
        is_private = true;
    }
    if (cache_mode.find("no-store")!=std::string::npos) {
        is_nostore = true;
    }
    if (cache_mode.find("no-cache")!=std::string::npos) {
        is_nocache = true;
    }
    if (cache_mode.find("s-maxage")!=std::string::npos) {
        is_maxage = true;
        std::size_t start=cache_mode.find("s-maxage");
        std::string s_max_age_str=cache_mode.substr(start+1);
        std::size_t pos = s_max_age_str.find("=");
        if (pos != std::string::npos) {
                       s_max_age_str=s_max_age_str.substr(pos + 1);
                       size_t stop=s_max_age_str.find(",");
                       if(stop != std::string::npos){
                       s_max_age_ = std::stol(s_max_age_str.substr(0,stop));}
                       else{ s_max_age_ = std::stol(s_max_age_str);}
        }
    }
    if (cache_mode.find("max-age")!=std::string::npos) {
        is_maxage = true;
        std::size_t start=cache_mode.find("max-age");
        std::string max_age_str=cache_mode.substr(start+1);
        std::size_t pos = max_age_str.find("=");
        if (pos != std::string::npos) {
                       max_age_str=max_age_str.substr(pos + 1);
                       size_t stop=max_age_str.find(",");
                       if(stop != std::string::npos){
                       max_age_ = std::stol(max_age_str.substr(0,stop));}
                       else{ max_age_ = std::stol(max_age_str);}
        }
    }
    if (cache_mode.find("must-revalidate")!=std::string::npos) {
        is_revalidate = true;
        }
    
}
void Response::time_manage(){
    time_t now=time(NULL)-28800;
    double now_age=difftime(now, date_);
    
    if(cache_mode.find("s-maxage")!=std::string::npos){
        expire_t=(time_t)(s_max_age_-now_age);
       is_fresh=s_max_age_>now_age;
    }
    else if(cache_mode.find("max-age")!=std::string::npos){
       expire_t=(time_t)(max_age_-now_age);
       is_fresh=max_age_>now_age;
    }
    else if(headers_map.find("Expires") != headers_map.end()){
    size_t pos = headers_map["Expires"].find(" GMT");
    expire_t= convert_time(headers_map["Expires"].substr(0,pos));
    is_fresh=expire_t>now;
    }
   
    // else if(headers_map.find("Last-Modified") != headers_map.end()){
    //         size_t pos = headers_map["Last-Modified"].find(" GMT");
    //         time_t last_modify= convert_time(headers_map["Last-Modified"].substr(0,pos));
    //         double fresh=difftime(date_, last_modify)/10.0;
    //         is_fresh=fresh>now_age;
    //         expire_t=(time_t)fresh;
    // }
    else{
        expire_="";
        is_fresh=true;
        return;
    
    }
    char expire_str[80];
    strftime(expire_str, 80, "%a, %d %b %Y %H:%M:%S GMT", gmtime(&expire_t));
    expire_= std::string(expire_str);
}
void Response::checkValidation(){
    if (headers_map.find("Cache-Control") != headers_map.end()){
        if (headers_map["Cache-Control"] == "must-revalidate" || headers_map["Cache-Control"] == "no-cache"){
            need_valiadate = true;
        }else{
            need_valiadate = false;
        }
    }
    if(is_fresh==false){need_valiadate = true;}
}

time_t Response::convert_time(const std::string& time_str) {
    struct tm gmttime;
    //memset(&gmttime, 0, sizeof(gmttime));
    strptime(time_str.c_str(), "%a, %d %b %Y %H:%M:%S %Z", &gmttime);
    time_t utctime = timegm(&gmttime);
    return utctime;
}


void Response::parse_header_map(const std::vector<std::string>& headers) {
    for (int i = 0; i < (int)headers.size(); ++i) {
           const std::string header = headers[i];
           std::size_t colon_pos = header.find(": ");
           if (colon_pos == std::string::npos) {
               // Invalid header format
               continue;
           }
           std::string key = header.substr(0, colon_pos);
           std::string value = header.substr(colon_pos + 2);
           key.erase(0, key.find_first_not_of(" "));
           key.erase(key.find_last_not_of(" ") + 1);
           headers_map[key] = value;
        
           //std::cout << "Header: " << key << " Value: " << headers_map[key] << std::endl;
       }
   
     if (headers_map.find("Content-Type") != headers_map.end()){
           content_type_=headers_map["Content-Type"];
     }
    if (headers_map.find("Content-Length") != headers_map.end()) {
           content_len_ = std::stoi(headers_map["Content-Length"]);
    }

      if (headers_map.find("ETag") != headers_map.end()){
              etag_=headers_map["ETag"];
     }
       if (headers_map.find("Cache-Control") != headers_map.end()){
        control_cache(headers_map["Cache-Control"]);
        cache_mode = headers_map["Cache-Control"];
     }
 
    if (headers_map.find("Transfer-Encoding") != headers_map.end()){
              if(headers_map["Transfer-Encoding"]=="chunked"){
                is_chunked=true;
              }
     }
    
    if (headers_map.find("Date") != headers_map.end()){
        size_t pos = headers_map["Date"].find(" GMT");
        std::string datestr_=headers_map["Date"].substr(0,pos);
        date_=convert_time(datestr_);
     }
    if(headers_map.find("Last-Modified") != headers_map.end()){
        size_t pos = headers_map["Last-Modified"].find(" GMT");
        last_modify= convert_time(headers_map["Last-Modified"].substr(0,pos));
        need_valiadate = true;
    }

}

