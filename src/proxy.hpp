#include "request.hpp"
#include "cache.hpp"
#include <mutex>
#include <iostream>
#include <fstream>
#include "server.hpp"
#include <thread>
#include <pthread.h>
#include <sys/select.h>

#define PORT 12345

static std::string Error502 = "HTTP/1.1 502 Bad Gateway\r\n\
Server: Apache/2.4.18 (Ubuntu)\r\n\
Connection: Closed\r\n\
Content-Type: text/html; charset=utf-8\r\n\r\n\
<!DOCTYPE HTML>\n\
<html>\n\
<head>\n\
<title>502 Bad Gateway Error</title>\n\
</head>\n\
<body>\n\
<h1>502 Bad Gateway Error</h1>\n\
<p>Sorry, the server encountered a bad gateway and could not fulfill your request.</p>\n\
</body>\n\
</html>";

static std::string Error400 = "HTTP/1.1 400 Bad Request\r\n\
Server: Apache/2.4.18 (Ubuntu)\r\n\
Connection: Closed\r\n\
Content-Type: text/html; charset=iso-8859-1\r\n\r\n\
<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\n\
<html>\n\
<head>\n\
<title>400 Bad Request</title>\n\
</head>\n\
<body>\n\
<h1>Bad Request</h1>\n\
<p>The request could not be understood by the server due to malformed syntax.</p>\n\
</body>\n\
</html>";

Response handleGet(Request& request, Client* client);
Response fetchNewResponse(Request& request, Client* client);
void handleConnect(Request& request, Client* client, int server_fd);
void handle_post(const Request* request,Client * client, int server_fd);
void start_proxy();
void * handle_request(void * arg);
int getNextChunkSize(const char* response, int len, const char** pos);
void writeToLog(std::string message);

class Arg{
    public:
    Client* client_ptr;
    int fd;
    Arg(int id, int fd_):_id(id){
        client_ptr = new Client(_id);
        fd = fd_;
    }
    ~Arg(){
        delete client_ptr;
    }
    private:
    int _id;   
};