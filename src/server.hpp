#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <cstring>

class Server{
    public:
    int status;
    int socket_fd;
    struct addrinfo host_info;
    struct addrinfo *host_info_list;
    const char *hostname;
    const char *port;
    Server(const char *port): port(port){
        startAsServer();
    }
    ~Server(){
        if(socket_fd!=0){
            close(socket_fd);
        }
    }
    void startAsServer();
    int acceptConnection();
    int acceptConnection(int socket_fd);
};

class Client{
    public:
    int status;
    int socket_fd;
    struct addrinfo host_info;
    struct addrinfo *host_info_list;
    int id;
    const char *hostname;
    const char *port;
    Client(int id, const char *hostname, const char *port): status(0),socket_fd(0),host_info_list(NULL),
        id(id), hostname(hostname), port(port){
        startAsClient();
    }
    Client(int id): id(id){}
    ~Client(){
        if(socket_fd!=0){
            close(socket_fd);
        }
    }
    void startAsClient();
    int initial_client(const char * hostname, const char * port);
};