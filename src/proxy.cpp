#include "proxy.hpp"

std::mutex thread_mutex;
std::ofstream logFile("/var/log/erss/proxy.log");

void writeToLog(std::string message){
    std::lock_guard<std::mutex> lock(thread_mutex);
    logFile << message << std::endl;
    logFile.flush();
}
Cache my_cache(100);
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
Response handleGet(Request& request, Client* client){
    Response response = my_cache.get_response(request.URI);
    if (!response.is_null) {
        // in cache
        if(response.get_need_valiadate() == false && response.get_is_fresh() == true){
            std::string message = std::to_string(client->id) + ": in cache, valid";
            writeToLog(message);
            return response;
        }else if(response.get_need_valiadate() == true){
            std::string message = std::to_string(client->id) + ": in cache, requires validation";
            writeToLog(message);
            Response newResponse = fetchNewResponse(request, client);
            return newResponse;
        }else if(response.get_is_fresh() == false){
            time_t expire_time = response.get_expire_t();
            struct tm* timeinfo = localtime(&expire_time);
            std::string message = std::to_string(client->id) + ": in cache, but expired at " + asctime(timeinfo);
            writeToLog(message);
            Response newResponse = fetchNewResponse(request, client);
            return newResponse;
        }
    }else{
        // not in cache
        std::string message = std::to_string(client->id) + ": not in cache";
        writeToLog(message);
        Response newResponse = fetchNewResponse(request, client);
        return newResponse;
    }
    return response;
}
// fetch new response from server: last-modified unchanged->return 304?不管了 no-store/private 不存
Response fetchNewResponse(Request& request, Client* client){
    std::string mess = request.requestInfo;
    //std::cout << "requestInfo : " << mess << std::endl;
    send(client->socket_fd, mess.c_str(), mess.size(), 0);
    std::string message1 = std::to_string(client->id) + ": Requesting \"" + request.req_line + "\" from " + request.URI;
    writeToLog(message1);
    // receive from server
    char oriResponse[65535];
    int res = recv(client->socket_fd, oriResponse, sizeof(oriResponse), 0);
    if(res <= 0){
        std::cout << "Error: cannot receive" << std::endl;
        exit(1);
    }
    Response newResponse(oriResponse);
    //std::cout << "Response content from server: "<< newResponse.get_raw_response() << std::endl;
    std::string message2 = std::to_string(client->id) + ": Received \"" + newResponse.get_status_line() + "\" from " + request.URI;
    writeToLog(message2);
    Response ori = my_cache.get_response(request.URI);
    if (!ori.is_null) {
        my_cache.remove_response(request.URI);
        my_cache.add_response(request.URI, newResponse);
    }else{   
        // determine whether need to cache
        if(newResponse.get_is_private() || newResponse.get_is_nostore()){
            if(newResponse.get_condition() == "200"){
                std::string message = std::to_string(client->id) + ": not cacheable because " + newResponse.cache_mode;
                writeToLog(message);
            }
            return newResponse;
        }else{
            my_cache.add_response(request.URI, newResponse);
        }
    }
    if(newResponse.get_condition() == "200"){
        if (newResponse.get_need_valiadate()){
            std::string message = std::to_string(client->id) + ": cached, but requires re-validation";
            writeToLog(message);
        }else if(newResponse.get_expire_t() > time(0)){
            time_t expire_time = newResponse.get_expire_t();
            struct tm* timeinfo = localtime(&expire_time);
            std::string message = std::to_string(client->id) + ": cached, expires at" + asctime(timeinfo);
            writeToLog(message);
        }    
    }
    return newResponse;
}

void handleConnect(Request& request, Client* client, int server_fd){
    std::string mess = "HTTP/1.1 200 OK\r\n\r\n";
    int send_len1 = send(server_fd, mess.c_str() , mess.size(), 0);
    if (send_len1 <= 0){
        perror("send");
        std::cout << "send failure" << std::endl;
        return;
    }
    std::string message = std::to_string(client->id) + ": Responding \"HTTP/1.1 200 OK\"";
    writeToLog(message);
    int client_fd = client->socket_fd;
    fd_set readfds;
    int maxfds = std::max(client_fd, server_fd) + 1;
    while(true){
        FD_ZERO(&readfds);
        FD_SET(server_fd, &readfds);
        FD_SET(client_fd, &readfds);
        select(maxfds, &readfds, NULL, NULL, NULL);
        char msg[65536];
        if(FD_ISSET(server_fd,&readfds)){
            int RecvBytes = recv(server_fd,msg,sizeof(msg),MSG_NOSIGNAL);
            if(RecvBytes <=0){
                return;
            }
            int SendBytes = send(client_fd,msg,RecvBytes,MSG_NOSIGNAL);
            if(SendBytes <=0){
                return;
            }
        }
        if(FD_ISSET(client_fd,&readfds)){
            int RecvBytes = recv(client_fd,msg,sizeof(msg),MSG_NOSIGNAL);
            if(RecvBytes <=0){
                return;
            }
            int SendBytes = send(server_fd,msg,RecvBytes,MSG_NOSIGNAL);
            if(SendBytes <=0){
                return;
            }
        }

    }
}

void handle_post(const Request* request,Client * client, int server_fd) {
    int client_fd=client->socket_fd;
    std::string request_str=request->requestInfo;
    send(client_fd,request_str.c_str(),request_str.size(), MSG_NOSIGNAL);
    std::string message1 = std::to_string(client->id) + ": Requesting \"" + request->req_line + "\" from " + request->URI;
        writeToLog(message1);
    // Receive the response from the server
    char response[65536];
    int response_len = recv(client_fd,&response, sizeof(response), MSG_WAITALL);

    /*if (response_len <= 0 || std::string(response.data(), response_len).find("\r\n\r\n") == std::string::npos) {
        logFile << cur_client->id << ": Responding \"HTTP/1.1 502 Bad Gateway\"" << std::endl;
        send(server_fd, Error502.c_str(), Error502.length(), 0);
        return;
    }*/
    Response res(response);
    // Log the response from server
    pthread_mutex_lock(&mutex);
    logFile <<client->id  << ": Received \"" << res.get_status_line() << "\" from " << request->get_hostname() << std::endl;
    pthread_mutex_unlock(&mutex);
      // Forward the response to the browser
   if (res.get_is_chunked()) {
    const char* pos = response;
    std::string chunked_response;
    while (true) {
        int chunk_size = getNextChunkSize(response, response_len, &pos);
        if (chunk_size <= 0) {
            break;
        }
        std::vector<char> chunk(chunk_size + 1, 0);
        int bytes_received = 0;
        do {
            int recv_result = recv(client_fd, chunk.data() + bytes_received, chunk_size - bytes_received, 0);
            if (recv_result <= 0) {
                logFile << client->id << ": Error receiving chunked response" << std::endl;
                send(server_fd, Error502.c_str(), Error502.length(), 0);
                return;
            }
            bytes_received += recv_result;
        } while (bytes_received < chunk_size);
        chunked_response += std::string(chunk.data(), chunk_size);
    }
    res.set_message(chunked_response);
    response_len = chunked_response.length();
   }
    // Forward the response to the browser
    send(server_fd, response, sizeof(response), MSG_NOSIGNAL);
    
    // Log responding
    pthread_mutex_lock(&mutex);
    logFile << client->id << ": Responding \"" << res.get_status_line() << std::endl;
    pthread_mutex_unlock(&mutex);
}

// Given a chunked response and a pointer to the current position,
// returns the size of the next chunk
// Returns -1 if there is an error in parsing the size
int getNextChunkSize(const char* response, int len, const char** pos) {
    // Search for the end of the current chunk
    const char* chunkEnd = *pos;
    while (chunkEnd < response + len && strncmp(chunkEnd, "\r\n", 2) != 0) {
        chunkEnd++;
    }
    if (chunkEnd == response + len) {
        return -1; // no more chunks
    }

    // Extract the chunk size as a hex number
    std::string chunkSizeString(*pos, chunkEnd - *pos);
    char* endptr;
    int chunkSize = strtol(chunkSizeString.c_str(), &endptr, 16);
    if (*endptr != '\0' || chunkSize < 0) {
        return -1;
    }

    // Update the position to point to the start of the next chunk
    *pos = chunkEnd + 2;
    // Return the size of the next chunk
    if (chunkSize == 0) {
        return chunkSize + 2;
    } else {
        return chunkSize + 2 + 2;
    }
}

void start_proxy() {
    Server server(std::to_string(PORT).c_str());
    //std::cout << "build server successfully" << std::endl;
    int id = 0;
    while (true) {
        // std::string ip;  
        int server_fd_new = server.acceptConnection();
        if (server_fd_new== -1) {
            std::string message = std::to_string(0) + ": ERROR cannot accpet client.";
            writeToLog(message);
            continue;
        }
        //std::cout << "server accept connection" << std::endl;
        pthread_t thread;
        pthread_mutex_lock(&mutex);
        Arg* arg_ptr = new Arg(id, server_fd_new);
        //std::cout << "Arg client id :" << arg_ptr->client_ptr->id << std::endl;
        ++id;
        pthread_mutex_unlock(&mutex);
        pthread_create(&thread, NULL, handle_request, (void*)arg_ptr);
    }

}

void * handle_request(void* arg){
    Arg* arg_ptr = (Arg*) arg;
    int server_fd = arg_ptr->fd;   // use to connect with browser(client)
    Client* cur_client = arg_ptr->client_ptr;
    //std::cout << "client id :" << cur_client->id << std::endl;

    char req[65536];
    int req_len = recv(server_fd, req, sizeof(req), 0);

    if (req_len <= 0) {
        logFile << cur_client->id << ": Error in Request" << std::endl;
        // delete cur_client;
        return NULL;
    }
    std::string input_str(req, req_len);
    Request request(input_str);
    try{
       request.parseRequest();
    }catch(InvalidRequest){
        pthread_mutex_lock(&mutex);
        logFile << cur_client->id << ": Responding \"HTTP/1.1 400 Bad Request\"" << std::endl;
        pthread_mutex_unlock(&mutex);
        send(server_fd, Error400.c_str(), Error400.length(), 0);
        return NULL;
    }
    //std::cout << "request :" << request.requestInfo << std::endl;
    if(request.method!= "GET" && request.method!= "POST" && request.method!= "CONNECT"){
        pthread_mutex_lock(&mutex);
        logFile << cur_client->id << ": Responding \"HTTP/1.1 400 Bad Request\"" << std::endl;
        pthread_mutex_unlock(&mutex);
        send(server_fd, Error400.c_str(), Error400.length(), 0);
    }
    else{
        const char* host_name = request.hostname.c_str();
        const char* port = request.port.c_str();
        cur_client->hostname = host_name;
        cur_client->port = port;
        cur_client->socket_fd = cur_client->initial_client(host_name, port);

        pthread_mutex_lock(&mutex);
        time_t currTime = time(0);
        const char * now_time = asctime(gmtime(&currTime));
        logFile << cur_client->id << ": \"" << request.method<< " "<< request.URI<< "\" from "<< cur_client->hostname << " @ " << now_time;
        pthread_mutex_unlock(&mutex);

        //cout << "received request:" << req << std::endl; 
        if(request.method == "GET") {
            if(request.port.empty()){
                request.port = "80";
            }
            Response newResponse = handleGet(request, cur_client);
            std::string mess = newResponse.get_raw_response();
            //std::cout << "Response return to client: "<< newResponse.get_raw_response() << std::endl;
            send(server_fd, mess.c_str(), mess.size(), 0);
            std::string message = std::to_string(cur_client->id) + ": Responding \"" + newResponse.get_status_line() + "\"";
            writeToLog(message);
        }else if (request.method == "POST") {
            if(request.port.empty()){
                request.port = "80";
            }
            handle_post(&request, cur_client, server_fd);  
        }else if (request.method == "CONNECT") {
            if(request.port.empty()){
                request.port = "443";
            }
            handleConnect(request, cur_client, server_fd);
            std::string message = std::to_string(cur_client->id) + ": Tunnel closed";
            writeToLog(message);
        }
    }

    delete arg_ptr;
    return NULL;
}
