#include "request.hpp"
namespace beast = boost::beast;
namespace http = beast::http;

void Request::parseRequest(){
  http::request_parser<http::string_body> parser;
  beast::error_code ec;
  parser.put(boost::asio::buffer(requestInfo), ec);
  if (ec) {
    std::cerr << "Error parsing HTTP request: " << ec.message() << std::endl;
    throw InvalidRequest();
  }
  // Extract the parsed HTTP request
  http::request<http::string_body> request = parser.get();
  beast::string_view view = request.method_string();
  std::string str(view.data(), view.size()); // Convert to std::string
  method = str;
  beast::string_view view2 = request.target();
  std::string str2(view2.data(), view2.size()); 
  URI = str2;
  req_line = method + " " + URI + " HTTP/" + std::to_string(request.version() / 10) + "." + 
  std::to_string(request.version() % 10);
  beast::string_view view3 = request.base().at("Host");
  std::string str3(view3.data(), view3.size()); 
  hostname = str3;
  // Default port is 80
  port = "80";  
  std::size_t colon_pos = hostname.find(':');
  if (colon_pos != std::string::npos) {
    port = hostname.substr(colon_pos + 1);
    hostname = hostname.substr(0, colon_pos);
  }
}
void Request::printRequest(){
  std::cout << "requestBody: " << request << std::endl;
  std::cout << "method: " << method << std::endl;
  std::cout << "URI: " << URI << std::endl;
  std::cout << "host: " << hostname << std::endl;
  std::cout << "port: " << port << std::endl;
}
// eg:  std::string request_str = "GET /index.html HTTP/1.1\r\nHost: example.com:8080\r\nUser-Agent: Boost Beast\r\n\r\n";
