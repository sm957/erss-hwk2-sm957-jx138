#include <string>
#include <boost/beast.hpp>
#include <cstring>
#include <iostream>
class Request{
    public:
    std::string requestInfo;
    std::string request;
    std::string method;
    std::string URI; 
    std::string port;
    std::string hostname;
    std::string req_line; 
    Request();  
    Request(std::string r){
        requestInfo = r;
        size_t index = requestInfo.find("HTTP/1.1");
        request = requestInfo.substr(0,index+8);
       
    }
    void parseRequest();
    void printRequest();
    std::string get_method() const { return method; }
    std::string get_URI() const { return URI; }
    std::string get_port() const { return port; }
    std::string get_hostname() const { return hostname; }
    std::string get_requestInfo() const { return requestInfo; }
    std::string get_req_line() const { return req_line; }
};

class InvalidRequest : public std::exception {
public:
    virtual const char* what() const noexcept {
        return "Invalid request";
    }
};