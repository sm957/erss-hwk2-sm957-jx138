#include "proxy.hpp"

int main() {
    pid_t pid = getpid();
    if (pid == 1) {
        pid = fork();
        if (pid != 0) {
            while (true) {
                
            }
        } else if (pid == -1) {
            std::cerr << "Fork system call failure" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    start_proxy();
}