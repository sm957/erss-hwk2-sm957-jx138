#include "server.hpp"
#include <memory.h>
#include <sys/socket.h>
#include <netdb.h>
#include <iostream>
#include <arpa/inet.h>
#define BACKLOG 100

void Server::startAsServer(){
    memset(&host_info, 0, sizeof(host_info));
    host_info.ai_family   = AF_UNSPEC;
    host_info.ai_socktype = SOCK_STREAM;
    host_info.ai_flags    = AI_PASSIVE;
    status = getaddrinfo(NULL, port, &host_info, &host_info_list);   
    if (status != 0) {
        std::cout << "Error: cannot get address info for host" << std::endl;
        exit(1);
    }
    // set a socket descriptor
    socket_fd = socket(host_info_list->ai_family, host_info_list->ai_socktype, host_info_list->ai_protocol);
    if (socket_fd == -1) {
        std::cout << "Error: cannot create socket" << std::endl;
        exit(1);
    } 
    int yes = 1;
    status = setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
    status = bind(socket_fd, host_info_list->ai_addr, host_info_list->ai_addrlen);
    if (status == -1) {
        std::cout << "Error: cannot bind socket" << std::endl;
        exit(1);
    }
    // listen, accept and receive
    status = listen(socket_fd, BACKLOG);
    if (status == -1) {
        std::cout << "Error: cannot listen on socket" << std::endl; 
        exit(1);
    }
    freeaddrinfo(host_info_list);
}

void Client::startAsClient(){
    std::cout << "ip: " << hostname << std::endl;
    std::cout << "port: " << port << std::endl;
    memset(&host_info, 0, sizeof(host_info));
    host_info.ai_family   = AF_UNSPEC;
    host_info.ai_socktype = SOCK_STREAM;
    status = getaddrinfo(hostname, port, &host_info, &host_info_list);
    if (status != 0) {
        std::cout << "Error: cannot get address info for host" << std::endl;
        exit(1);
    }
    // set a socket descriptor
    socket_fd = socket(host_info_list->ai_family, host_info_list->ai_socktype, host_info_list->ai_protocol);
    if (socket_fd == -1) {
        std::cout << "Error: cannot create socket" << std::endl;
        exit(1);
    }
    status = connect(socket_fd, host_info_list->ai_addr, host_info_list->ai_addrlen);
    if (status == -1) {
        std::cout << "Error: cannot connect to socket" << std::endl;
        exit(1);
    }
    freeaddrinfo(host_info_list);
}

int Server::acceptConnection(){
    struct sockaddr_storage socket_addr;
    socklen_t socket_addr_len = sizeof(socket_addr);;
    int socket_fd_new = accept(socket_fd, (struct sockaddr *)&socket_addr, &socket_addr_len); 
    if (socket_fd == -1) {
        std::cout << "Error: cannot accept connection on socket" << std::endl;
        exit(1);
    }
    return socket_fd_new;
}

int Server::acceptConnection(int socket_fd){
    struct sockaddr_storage socket_addr;
    socklen_t socket_addr_len = sizeof(socket_addr);
    int socket_fd_new;
    socket_fd_new= accept(socket_fd, (struct sockaddr *)&socket_addr, &socket_addr_len);
    if(socket_fd_new==-1){
        std::cout << "Error: cannot accept connection on socket" << std::endl;
        exit(1);
    }
    struct sockaddr_in * addr = (struct sockaddr_in *)&socket_addr;
    std::cout << "accepting ip address: "<<inet_ntoa(addr->sin_addr) << std::endl;
    std::string ip = inet_ntoa(addr->sin_addr);
  
    return socket_fd_new;
}
int Client::initial_client(const char * hostname, const char * port){
    int status;
    int socket_fd;
    struct addrinfo host_info;
    struct addrinfo *host_info_list;


    memset(&host_info, 0, sizeof(host_info));
    host_info.ai_family   = AF_UNSPEC;
    host_info.ai_socktype = SOCK_STREAM;

    status = getaddrinfo(hostname, port, &host_info, &host_info_list);
    if (status != 0) {
        std::cerr << "Error: cannot get address info for host" << std::endl;
        std::cerr << "  (" << hostname << "," << port << ")" << std::endl;
        return -1;
    } //if

    socket_fd = socket(host_info_list->ai_family,
                       host_info_list->ai_socktype,
                       host_info_list->ai_protocol);
    if (socket_fd == -1) {
        std::cerr << "Error: cannot create socket" << std::endl;
        std::cerr << "  (" << hostname << "," << port << ")" << std::endl;
        return -1;
    } //if

    // cout << "Connecting to " << hostname << " on port " << port << "..." << endl;

    status = connect(socket_fd, host_info_list->ai_addr, host_info_list->ai_addrlen);
    if(status == -1){
        std::cerr << "Error: cannot connect to socket" << std::endl;
        std::cerr << "  (" << hostname << "," << port << ")" << std::endl;
        return -1;
    } //if
    freeaddrinfo(host_info_list);

    return socket_fd;
}