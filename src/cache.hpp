#ifndef CACHE_HPP
#define CACHE_HPP

#include <map>
#include <mutex>
#include <string>
#include <list>
#include "response.hpp"

class Cache {
 public:
  explicit Cache(size_t max_size) : max_size_(max_size), size_(0) {}
  ~Cache(){
    clear();
  }
  void add_response(const std::string& key, const Response& response);
  Response get_response(const std::string& key);
  void remove_response(const std::string& key);
  void clear();

  struct ResponseEntry {
    Response response;
    std::list<std::string>::iterator order_iterator;
  };

  size_t max_size_;
  size_t size_;
//Used to record the response in cache in map
  std::map<std::string, ResponseEntry> cache_;

//Used to record the order of response
  std::list<std::string> order_;
  std::mutex mutex_;
};

#endif  // CACHE_HPP
