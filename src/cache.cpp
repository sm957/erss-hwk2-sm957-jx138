
#include "cache.hpp"


void Cache::add_response(const std::string& key, const Response& response) {
  std::lock_guard<std::mutex> lock(mutex_);
  std::map<std::string, ResponseEntry>::iterator it = cache_.find(key);
  if (it != cache_.end()) {
    // If the key is already in the cache, update the response and move it to the front of the list
    it->second.response = response;
    order_.splice(order_.begin(), order_, it->second.order_iterator);
  } else {
    // If the key is not in the cache, add the new response to the front of the list and insert it into the cache
    order_.push_front(key);
    std::list<std::string>::iterator iter = order_.begin();
    cache_.insert({key, {response, iter}});
    size_ ++;
  }
  // If the cache is full, remove the least recently used response from the cache
  while (cache_.size() > max_size_) {
    std::string last_key = order_.back();
    ResponseEntry last_entry = cache_[last_key];
    size_ --;
    cache_.erase(last_key);
    order_.pop_back();
  }
}

Response Cache::get_response(const std::string& key) {
  std::lock_guard<std::mutex> lock(mutex_);
  std::map<std::string, ResponseEntry>::iterator it = cache_.find(key);
  if (it != cache_.end()) {
    // If the key is in the cache, move it to the front of the list and return the response
    order_.splice(order_.begin(), order_, it->second.order_iterator);
    return it->second.response;
  } else {
    return Response();  //return empty response
  }
}

void Cache::remove_response(const std::string& key) {
  std::lock_guard<std::mutex> lock(mutex_);
  std::map<std::string, ResponseEntry>::iterator it = cache_.find(key);
  if (it != cache_.end()) {
    size_ --;
    order_.erase(it->second.order_iterator);
    cache_.erase(it);
  }
}

void Cache::clear() {
  std::lock_guard<std::mutex> lock(mutex_);
  cache_.clear();
  order_.clear();
  size_ = 0;
}
